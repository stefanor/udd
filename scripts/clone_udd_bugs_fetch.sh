#!/bin/sh

set -e

TARGETDIR=/srv/udd.debian.org/mirrors/clone-bugs
FETCHURL=http://udd.debian.org/udd-bugs.sql.xz

rm -rf $TARGETDIR
mkdir -p $TARGETDIR
cd $TARGETDIR

# work around a regression in wget from wheezy to jessie
# see https://wiki.debian.org/ServicesSSL#wget
WGET=wget
dir=/etc/ssl/ca-debian
test -d $dir && WGET="wget --ca-directory=$dir"

$WGET -q ${FETCHURL}
